package lk.autostreet.services.api.exception;

public class CityNotFoundException extends Exception {

    public CityNotFoundException(String message) {
        super(message);
    }
}
