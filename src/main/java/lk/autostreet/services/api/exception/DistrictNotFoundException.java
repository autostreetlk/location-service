package lk.autostreet.services.api.exception;

public class DistrictNotFoundException extends Exception {

    public DistrictNotFoundException(String message) {
        super(message);
    }
}
